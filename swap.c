#include <stdio.h>
void call_by_reference(int*,int*);
int main()
{
  int a,b;
  printf("value of a&b before swap");
  call by reference(&a,&b);
  printf("value of a&b after swap=%d %d",a,b);
  return 0;
}
void call_by_reference(int*m,int*n)
{
  int temp;
  temp=*m;
  *m=*n;
  *n=temp;
  printf("in function,a=%d,b=%d",*m,*n);
}  